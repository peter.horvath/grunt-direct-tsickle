# grunt-direct-tsickle
Grunt task to call tsickle, as from the command line, but without forking a new node instance.

Watchmode (`-w`) doesn't work yet, but if you file an issue, I will implement
it in days.

### Example:

    module.exports = function(grunt) {
      'use strict';
      grunt.initConfig({
    
        tsickle : {
          dist : {
            options : {
              args : [ "--externs=dist/externs.js", "--", "-p", "src" ],
              debug : true
            }
          },
          test : {
            options : {
              args : [ "--externs=dist/externs-test.js", "--", "-p", "test/src" ],
              debug : true
            }
          }
        }

      });
      grunt.loadNpmTasks("grunt-direct-tsickle");
    };

Here we can see multiple configurations in which we can call the typescript
compiler. I think it is nearly self-explaining:

`grunt tsickle:dist` will be the equivalent of a `./node_modules/.bin/tsickle -p tsconfig-dist.json`,
while `grunt tsickle:dev` will do an equivalent of `./node_modules/.bin/tsickle -p tsconfig-test.json`.

There are no more configuration options.
